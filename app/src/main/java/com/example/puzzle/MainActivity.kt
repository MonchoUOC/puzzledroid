package com.example.puzzle

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.puzzle.R.drawable.ic_baseline_favorite_24

class MainActivity : AppCompatActivity() {
    private var favorito = false


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Puzzle)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        /* Metodo para ir a la nueva actividad donde estaran las imagenes para seleccionar*/


        //Fución  que muestra y oculta el menú
    }
    private fun setFavoriteIcon(menuItem: MenuItem){
        val id = if(favorito) ic_baseline_favorite_24;
        else R.drawable.ic_baseline_favorite_border_24;

        menuItem.icon = ContextCompat.getDrawable(this,id)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.favorito -> {
                favorito = !favorito
                setFavoriteIcon(item)
            }
            R.id.compartir ->{
                val sendIntent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, "https://gitlab.com/MonchoUOC/puzzle.git")

                    type = "text/plain"
                }
                val shareIntent = Intent.createChooser(sendIntent,null)
                startActivity(shareIntent)

            }
            R.id.salir ->{
                finish()
            }
            R.id.ayuda -> {
                Toast.makeText(applicationContext, "Ayuda", Toast.LENGTH_LONG).show()
                val myWebView = WebView(applicationContext)
                setContentView(myWebView)
                myWebView.loadUrl("file:///android_asset/juego.html")
                // myWebView.loadUrl("https://www.google.com")
                true
            }
        }
        return super.onOptionsItemSelected(item)
        }




    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.barra, menu)
        return true
    }

    // Función que asigna las opciones del menu
/*
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.ayuda -> {
                Toast.makeText(applicationContext, "Ayuda", Toast.LENGTH_LONG).show()
                val myWebView = WebView(applicationContext)
                setContentView(myWebView)
                myWebView.loadUrl("file:///android_asset/juego.html")
                // myWebView.loadUrl("https://www.google.com")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }


    }
    */
    /*override fun onBackPressed() {
        val myWebView = WebView(applicationContext)
        if (myWebView.canGoBack()) {
            myWebView.goBack()
        } else {
            super.onBackPressed()
        }
    }*/

    /*
    Función de retroceso al menu e inicio
    https://www.youtube.com/watch?v=bTpYXvcpbro
     */
    override fun onBackPressed() {
        super.onBackPressed()
        //Toast.makeText(applicationContext, "Ayuda", Toast.LENGTH_LONG).show()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    /* Funcion salir aplicación*/
    fun exit(view: View) {
        System.exit(0);
    }

    fun imagenesJuego(view: View) {
        val intent = Intent(this, imagenesJuego::class.java)
        startActivity(intent)
    }

}