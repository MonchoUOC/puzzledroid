package com.example.puzzle

import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton

class imagenesJuego : AppCompatActivity(), View.OnClickListener {
    var i: Intent? = null
    var mp: MediaPlayer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_imagenes_juego)
        val sonidoOn = findViewById<View>(R.id.sonidoOn) as Button
        val sonidoOff = findViewById<View>(R.id.sonidoOff) as Button
        sonidoOn.setOnClickListener(this)
        sonidoOff.setOnClickListener(this)

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.sonidoOn -> if (!reproduciendo) { //verifica que ya no se este reproduciendo
                mp = MediaPlayer.create(this, R.raw.musica)
                with(mp) {
                    this?.setLooping(true)
                    this?.start()
                }
                reproduciendo = true
            }
            R.id.sonidoOff -> if (reproduciendo) {
                mp!!.stop()
                mp!!.isLooping = false
                reproduciendo = false
            }

        }
    }

    companion object {
        var reproduciendo = false
    }

    fun OnclickImagen(view: View) {

        val tigre: ImageButton = findViewById(R.id.tigre)
        val nieve: ImageButton = findViewById(R.id.nieve)
        val girasol: ImageButton = findViewById(R.id.girasol)
        val intent = Intent(this, Nivel1::class.java)
        startActivity(intent)
        tigre.setEnabled(false)
        nieve.setEnabled(false)
        girasol.setEnabled(false)





        }


    }
